// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

//
// Example #1
//
A = [
6 2 3
2 5 1
3 1 3
];
b = [1 1 1]';
mu = 1;
[mu,b,iter]=linalg_rayleighiteration ( A , mu , b );
assert_checkalmostequal(mu,1.1330107);
be = [0.5425814  ;    -0.0640301  ;    -0.8375593 ];
assert_checkalmostequal(b,be,1.e-6,[],"element");
r = A*b-mu*b;
assert_checkalmostequal(r,zeros(3,1),[],1.e-5);
// Configure options
rtol = 1.e-4;
itmax = 20;
verbose = %f;
[mu,b,iter]=linalg_rayleighiteration ( A , mu , b , rtol , itmax , verbose );
// See what happens
[mu,b,iter]=linalg_rayleighiteration ( A , mu , b , rtol , itmax , %t );

//
// Example #2
//
A = [
1 2 3
1 2 1
3 2 1
];
b = [1 1 1]';
mu = 200;
[mu,b,iter]=linalg_rayleighiteration ( A , mu , b );
assert_checkalmostequal(mu,5.2360679774997826996241);
be = [
  0.6479361632942985949057;
  0.4004465714560787037435;
  0.6479361632942985949057;
];
assert_checkalmostequal(b,be,1.e-6,[],"element");
r = A*b-mu*b;
assert_checkalmostequal(r,zeros(3,1),[],1.e-3);
//
// Example #3
// The initial eigenvalue is close to the
// exact one.
// In the previous version, this made Scilab switch to
// least squares and prints a warning.
// Now we use linalg_gesv and this works fine.
//

A = [
1 2 3
1 2 1
3 2 1
];
b = [1 1 1]';
mu = 0.7639320225002093067701;
[mu,b,iter]=linalg_rayleighiteration ( A , mu , b );
assert_checkalmostequal(mu,0.7639320,1.e-4);
be = [
   0.4653411;
  -0.7529378;
   0.4653411
];
assert_checkalmostequal(b,be,1.e-4,[],"element");
r = A*b-mu*b;
assert_checkalmostequal(r,zeros(3,1),[],1.e-4);


