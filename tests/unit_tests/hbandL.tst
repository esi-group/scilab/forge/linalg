// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



// Test band conversion
B = [
3 1+7*%i 0
1-7*%i 4 -2-8*%i
0 -2+8*%i -12
];
nsubdiag = 1;
A = linalg_hbandL ( B , nsubdiag) ;
Ae = [
3 4 -12
1-7*%i -2+8*%i 0
];
assert_checkequal ( A , Ae );

//
// A larger example
n = 10;
nsubdiag = 5;
Ar = zeros(n,n);
for i = 1 : n
  j = (i:i+nsubdiag-1);
  j = max(min(j,n),1);
  Ar(i,j) = j;
end
Ai = zeros(n,n);
for i = 1 : n
  j = (i:i+nsubdiag-1);
  j = max(min(j,n),1);
  Ai(i,j) = j;
end
A = (Ar+%i*Ai) + (Ar'-%i*Ai');
// Convert it into band-form
AB=linalg_hbandL(A,nsubdiag);
expected = [
 2, 4, 6, 8, 10, 12, 14, 16, 18, 20;
 2-%i*2, 3-%i*3, 4-%i*4, 5-%i*5, 6-%i*6, 7-%i*7, 8-%i*8, 9-%i*9, 10-%i*10,0;
 3-%i*3, 4-%i*4, 5-%i*5, 6-%i*6, 7-%i*7, 8-%i*8, 9-%i*9, 10-%i*10,0,0;
 4-%i*4, 5-%i*5, 6-%i*6, 7-%i*7, 8-%i*8, 9-%i*9, 10-%i*10,0,0,0;
 5-%i*5, 6-%i*6, 7-%i*7, 8-%i*8, 9-%i*9, 10-%i*10,0,0,0,0;
0,0,0,0,0,0,0,0,0,0
];
assert_checkequal ( AB , expected );

