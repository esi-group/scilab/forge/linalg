// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// One RHS in the linear system of equations
A=  [
1 2
3 4
] + %i* [
5 6
7 8];
e = [5;6] + %i*[7;8];
b = [-66;-74] + %i * [84;136];
x = linalg_zgesv(A,b);
assert_checkalmostequal(x,e,1.e2*%eps);
//
// 3 RHS in the linear system of equations
A=  [
1 2
3 4
] + %i* [
5 6
7 8];
e = [5,1,2,3;6,2,3,4]+%i*[7,8,9,10;8,9,10,11];
b = [-66,-89,-97,-105;-74,-117,-125,-133] + %i*[84,43,57,71;136,83,105,127];
x = linalg_zgesv(A,b);
assert_checkalmostequal(x,e,1.e2*%eps);
// A singular system
A = [1 2
     1 2] ..
  + %i* [4 3
         4 3];
b = [-66;-74] + %i * [84;136];
instr = "x = linalg_zgesv(A,b)";
localmsg = msprintf(gettext("%s: Matrix is singular."),"linalg_zgesv");
assert_checkerror(instr,localmsg);


