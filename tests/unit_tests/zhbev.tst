// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


AB = [
  1 2 3;
  4 5 0;
  6 0 0
];
AB = complex(AB,0);
computed = linalg_zhbev(AB);
expected = [
  -4.094602
  -2.0337914
   12.128393
];
assert_checkalmostequal ( computed , expected , 1.e-7 );
//
// The equivalent full matrix:
//
A = [
1 4 6
4 2 5
6 5 3
];
A = complex(A,0);
expected = spec(A);
expected = gsort(expected,"g","i");
assert_checkalmostequal ( computed , expected , 10 * %eps );

