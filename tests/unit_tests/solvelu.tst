// Copyright (C) 2008 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



// Simple case
L=[
1 0 0 0 0
1 1 0 0 0
1 1 1 0 0
1 1 1 1 0
1 1 1 1 1
];
U=[
1 1 1 1 1
0 1 1 1 1
0 0 1 1 1
0 0 0 1 1
0 0 0 0 1
];
b=[5;9;12;14;15];
xe=[1;1;1;1;1];
x=linalg_solvelu(L,U,b);
assert_checkalmostequal ( x , xe , %eps );
// See what happens
x=linalg_solvelu(L,U,b,%t);
