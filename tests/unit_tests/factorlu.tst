// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



//
// Test A=LU with simple case
//
A=[
1 1 1 1 1
1 2 2 2 2
1 2 3 3 3
1 2 3 4 4
1 2 3 4 5
];
verbose = 1;
[L,U]=linalg_factorlu(A);
Lexpected=[
1 0 0 0 0
1 1 0 0 0
1 1 1 0 0
1 1 1 1 0
1 1 1 1 1
];
assert_checkalmostequal ( L , Lexpected ,  %eps );
Uexpected=[
1 1 1 1 1
0 1 1 1 1
0 0 1 1 1
0 0 0 1 1
0 0 0 0 1
];
assert_checkalmostequal ( U , Uexpected ,  %eps );
// See verbose in action
[L,U]=linalg_factorlu(A,%t);

//
// Test Ax=b with A=LU decomposition
//
A=[
1 1 1 1 1
1 2 2 2 2
1 2 3 3 3
1 2 3 4 4
1 2 3 4 5
];
b=[5;9;12;14;15];
[L,U]=linalg_factorlu(A);
x=linalg_solvelu(L,U,b,%f);
xexpected=[1;1;1;1;1];
assert_checkalmostequal ( x , xexpected ,  %eps );

//
// Test A=LU with difficult case
//
A=[
1.d-8 1
1     1
];
verbose = 1;
[L,U]=linalg_factorlu(A);
Lexpected=[
1.   0
1.d8 1.];
assert_checkalmostequal ( L , Lexpected ,  %eps );
Uexpected=[
1.d-8  1
0.    -1.d8+1
];
assert_checkalmostequal ( U , Uexpected ,  %eps );
// cond(A) : 2.6180340238745070102766
// cond(L) : 9999999949752408.
// cond(U) : 9999999900000000.

