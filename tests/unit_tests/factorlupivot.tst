// Copyright (C) 2008 - 2010 - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



//
// Test PA=LU with difficult case
//
A=[
1.d-8 1
1     1
];
verbose = 0;
[L,U,P]=linalg_factorlupivot(A);
Lexpected=[
1.    0
1.d-8 1.
];
assert_checkalmostequal ( L , Lexpected , %eps );
Uexpected=[
1     1
0     1 - 1.d-8
];
assert_checkalmostequal ( U , Uexpected , %eps );
Pexpected=[
0 1
1 0
];
assert_checkalmostequal ( P , Pexpected , %eps );
assert_checkalmostequal ( P*A , L*U , %eps );
// cond(A) : 2.6180340238745070102766
// cond(L) : 1.0000000099999999392253
// cond(U) : 2.6180340004580986423832
format(10, 'v');
[L,U,P]=linalg_factorlupivot(A,%f);


//
// Test Ax=b with PA=LU decomposition
//
A=[
1 1 1 1 1
1 2 2 2 2
1 2 3 3 3
1 2 3 4 4
1 2 3 4 5
];
b=[
5
9
12
14
15
];
[L,U,P]=linalg_factorlupivot(A);
x=linalg_solvelu(L,U,P*b,%f);
xexpected=[
1
1
1
1
1
];
assert_checkalmostequal ( x , xexpected , %eps );


// Test PA=LU with extremely difficult case
//
A=[
2*%eps 1
1      1
];
verbose = 1;
[L,U,P]=linalg_factorlupivot(A);
Lexpected=[
1.    0
2*%eps 1.
];
assert_checkalmostequal ( L , Lexpected , %eps );
Uexpected=[
1     1
0     1 - 2*%eps
];
assert_checkalmostequal ( U , Uexpected , %eps );
Pexpected=[
0 1
1 0
];
assert_checkalmostequal ( P , Pexpected , %eps );
assert_checkalmostequal ( P*A , L*U , %eps );

