// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



if ( %f ) then
  // How to create such a matrix
  R = qr(rand(5,5));
  D = diag((1:5));
  L = R';
  A = R*D*L;
end

e = [
  1
  2
  3
  4
  5
];
A = [
  2.981136978768197388D+00,-1.521235057732858720D-01,-1.494446478130108780D+00, 3.455814188466865833D-02,3.801435242411201898D-01;
  -1.521235057732858720D-01,1.643377375064559942D+00,-5.913719198065179317D-01, -5.355744671392123069D-01,2.280578576621679066D-01;
  -1.494446478130108780D+00,-5.913719198065179317D-01,3.150577563873246945D+00, -1.800738655149962952D-01,-7.080430589262648455D-01;
  3.455814188466863057D-02,-5.355744671392123069D-01,-1.800738655149962952D-01, 3.874373011768809771D+00,5.779580504744613600D-03;
  3.801435242411201898D-01,2.280578576621679066D-01,-7.080430589262648455D-01, 5.779580504744613600D-03,3.350535070525185510D+00
];
d = linalg_dsyev(A);
d = gsort(d,"g","i");
assert_checkalmostequal ( d , e , 10*%eps );
//
// Get the eigenvectors too
[R,d] = linalg_dsyev ( A );
[d,k] = gsort(d,"g","i");
assert_checkalmostequal ( d , e , 10*%eps );
R = R(:,k);
RE = [
-0.4423173,0.5401228,0.4248701,-0.0075838,-0.5762383;
-0.7115418,-0.6505556,-0.0598693,0.2337072,-0.1108252;
-0.5217547,0.4693522,-0.2228218,-0.0183584,0.676383;
-0.1599162,-0.1513843,-0.0936001,-0.9680189,-0.0754186;
-0.0162022,0.2045387,-0.8703393,0.0890265,-0.4387314
];
//assert_checkalmostequal ( R , RE , 1.e-6 );
assert_checkalmostequal ( R , RE , 1.e-5 );

