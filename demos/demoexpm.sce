// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Compute the parameters p and q which can be used for Pad� approximants
// in the matrix exponential.

function delta = expmrtol(p, q)
    // The relative error on the matrix norm.
    // Bibliography
    //   "Matrix Computations", Golub and Van Loan, Third Edition,
    //   The Johns Hopkins University Press, p573, epsilon(p,q)
    delta = 2 .^(3-p-q) .* factorial(p) .*factorial(q) ..
          ./ factorial(p+q) ./ factorial(p+q+1);
endfunction

// Compute (p,q) such that epsilon(p,q) < %eps
for q = 1:7
    delta = expmrtol(q, q);
    disp([q q delta]);
end
// 7.    7.    1.088D-19
// Conclusion p=q=7 is about %eps.

// Load this script into the editor
filename = "demoexpm.sce";
dname = get_absolute_file_path(filename);
editor(fullfile(dname,filename));

