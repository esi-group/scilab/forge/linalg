// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function D = linalg_zgemm ( Alpha , A , B , Beta , C)
  // Computes D = Alpha*A*B+ Beta*C for a complex matrix.
  //
  // Calling Sequence
  //   D = linalg_zgemm ( Alpha , A , B , Beta , C)
  //
  // Parameters
  // Alpha : a 1-by-1 matrix of complex doubles
  // A : a m-by-n matrix of complex doubles
  // B : a n-by-p matrix of complex doubles
  // Beta : a 1-by-1 matrix of complex doubles
  // C : a m-by-p matrix of complex doubles
  // D : a m-by-p matrix of complex doubles
  //
  // Description
  //   Calls BLAS/ZGEMM.
  //
  // Examples
  // Alpha = 1 + %i;
  // Beta = Alpha;
  // A = ones(2,2) + %i * ones(2,2);
  // B = A;
  // C = A;
  // D = linalg_zgemm(Alpha, A, B, Beta, C)
  // // Compare with Scilab's:
  // Alpha*A*B+ Beta*C
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

endfunction


