// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function D = linalg_dsyev ( A )
  // Computes the eigenvalues of a symmetric real matrix.
  //
  // Calling Sequence
  //   d = linalg_dsyev ( A )
  //   [R,d] = linalg_dsyev ( A )
  //
  // Parameters
  // A : a n-by-n matrix of doubles (real)
  // d : a n-by-1 matrix of doubles (real), the eigenvalues
  // R : a n-by-n matrix of doubles (real), the eigenvectors
  //
  // Description
  //   Computes the eigenvalues of a symmetric real matrix by calling LAPACK/DSYEV.
  //
  // Examples
  // expected = (1:5)'
  // A = [
  //   2.981136978768197388D+00,-1.521235057732858720D-01,-1.494446478130108780D+00, 3.455814188466865833D-02,3.801435242411201898D-01;
  //   -1.521235057732858720D-01,1.643377375064559942D+00,-5.913719198065179317D-01, -5.355744671392123069D-01,2.280578576621679066D-01;
  //   -1.494446478130108780D+00,-5.913719198065179317D-01,3.150577563873246945D+00, -1.800738655149962952D-01,-7.080430589262648455D-01;
  //   3.455814188466863057D-02,-5.355744671392123069D-01,-1.800738655149962952D-01, 3.874373011768809771D+00,5.779580504744613600D-03;
  //   3.801435242411201898D-01,2.280578576621679066D-01,-7.080430589262648455D-01, 5.779580504744613600D-03,3.350535070525185510D+00
  // ];
  // d = linalg_dsyev(A)
  // d = gsort(d,"g","i")
  // // Compare with Scilab's:
  // spec(A)
  //
  // // Get the eigenvectors too
  // [R,d] = linalg_dsyev ( A )
  // // Compare with Scilab's:
  // [R,D] = spec(A)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

endfunction


