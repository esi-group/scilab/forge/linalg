// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function B = linalg_powfast ( A , p )
  // Computes A^p
  //
  // Calling Sequence
  //  B = linalg_powfast ( A , p )
  //
  // Parameters
  // A : a n-by-n matrix of doubles, real, dense.
  // p : a 1-by-1 matrix of doubles, real
  // B : a n-by-n matrix of doubles, B=A^p
  //
  // Description
  //   Uses a binary algorithm to compute the power of the matrix A.
  //   This algorithm requires generally log2(p) iterations, which
  //   is (much) faster that Scilab v5 power algorithm for real matrices.
  //   It is based on a compiled source code and it is fast.
  //
  // Examples
  // A = [
  //     5.    4.    3.    2.    1.
  //     4.    4.    3.    2.    1.
  //     0.    3.    3.    2.    1.
  //     0.    0.    2.    2.    1.
  //     0.    0.    0.    1.    1.
  // ];
  // B = linalg_powfast(A,5)
  // E = [
  //     37721.    47455.    45750.    34540.    18160.
  //     33940.    42751.    41245.    31150.    16380.
  //     15420.    19695.    19156.    14525.    7650.
  //     3720.     5010.     5020.     3861.     2045.
  //     360.      570.      620.      495.      266.
  // ];
  //
  // Authors
  // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
  //
  // Bibliography
  // D. Knuth, "The Art of Computer Programming", Vol. 2, "Seminumerical Algorithms"
  // http://bugzilla.scilab.org/show_bug.cgi?id=8428

endfunction


