// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating linalg\n");
helpdir = fullfile(cwd);
funmat = [
  "linalg_chol"
  "linalg_factorlu"
  "linalg_factorlupivot"
  "linalg_gaussnaive"
  "linalg_gausspivotal"
  "linalg_gausspivotalnaive"
  "linalg_solvelu"
  "linalg_hbandL"
  "linalg_expm"
  "linalg_condeig"
  "linalg_rayleighiteration"
  "linalg_pow"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "linalg";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the help for gateways
mprintf("Updating linalg/gateways\n");
helpdir = fullfile(cwd);
funmat = [
  "linalg_powfast"
  ];
macrosdir = fullfile(cwd,"pseudomacros");
demosdir = [];
modulename = "linalg";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the help for BLAS/LAPACK gateways
mprintf("Updating linalg/blaslapack/gateways\n");
helpdir = fullfile(cwd,"blaslapack");
funmat = [
  "linalg_dgemm"
  "linalg_zgemm"
  "linalg_dsyev"
  "linalg_zhbev"
  "linalg_dgesv"
  "linalg_zgesv"
  ];
macrosdir = fullfile(cwd,"pseudomacros");
demosdir = [];
modulename = "linalg";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


//
// Generate the help for BLAS/LAPACK macros
mprintf("Updating linalg/blaslapack/gateways\n");
helpdir = fullfile(cwd,"blaslapack");
funmat = [
  "linalg_gesv"
  "linalg_gemm"
  ];
macrosdir = fullfile(cwd ,"..","..","macros");
demosdir = [];
modulename = "linalg";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


