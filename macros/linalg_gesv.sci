// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = linalg_gesv(A, B)
    // Computes the real or complex solution X of A*X = B.
    //
    // Calling Sequence
    //   X = linalg_gesv(A, B)
    //
    // Parameters
    // A : a n-by-n matrix of doubles (real or complex)
    // B : a n-by-p matrix of doubles (real or complex)
    // X : a n-by-p matrix of doubles (real or complex),
    //     the solution of A*X = B.
    //
    // Description
    //   Computes the real or complex solution X of A*X = B.
    //   Uses BLAS/DGESV if all inputs are real,
    //   uses ZGESV if any input is complex.
    //   Convert to complex matrices when necessary to call ZGESV.
    //
    // The solution of A*X = B in Scilab is based on the backslash operator.
    // The backslash operator is switching from Gaussian Elimination(with
    // pivoting)to Linear Least Squares when the condition number of
    // the matrix is larger than roughly 10^8.
    // This switch is annoying for matrices which condition number are
    // between 10^8 and 10^16, where the Gaussian Elimination can
    // produce a more accurate result than Least Squares.
    // The linalg_gesv function provided in this module is a direct
    // interface to the Gaussian Elimination from Lapack.
    // In some cases, this function can produce significantly more accurate
    // solution than backslash.
    // See the example below for the Hilbert matrix of size 8.
    //
    // Examples
    // // A real system of equations
    // A = [1 2
    //      3 4];
    // e = [5 1 2 3
    //      6 2 3 4];
    // b = [17  5  8 11
    //      39 11 18 25];
    // x = linalg_gesv(A, b)
    // // A mixed system
    // A = [1 2
    //      3 4] ..
    //   + %i * [5 6
    //          7 8];
    // e = [5
    //      6];
    // b = [17
    //      39] ..
    //   + %i * [61
    //           83];
    // x = linalg_gesv(A, b)
    //
    // // Compare with Scilab for difficult matrix
    // n = 8;
    // e = ones(n, 1);
    // A = testmatrix("hilb", n);
    // b = A*e;
    // // With backslash
    // x = A\b;
    // norm(A*x-b) / norm(b)
    // norm(x-e) / norm(e)
    // // With gesv
    // x = linalg_gesv(A, b);
    // norm(A*x-b) / norm(b)
    // norm(x-e) / norm(e)
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    function B = linalg_complexify(A)
        if isreal(A) then
            B = complex(A);
        else
            B = A;
        end
    endfunction

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_gesv", rhs, 2:2);
    apifun_checklhs("linalg_gesv", lhs, 0:1);

    // Check type
    apifun_checktype("linalg_gesv", A, "A", 1, "constant");
    apifun_checktype("linalg_gesv", B, "B", 2, "constant");

    // Check size
    [mA nA] = size(A);
    [mB nB] = size(B);
    apifun_checkdims("linalg_gesv", A, "A", 2, [mA mA]);
    apifun_checkdims("linalg_gesv", B, "B", 2, [mA nB]);

    // Switch to DGESV/ZGESV
    isRealA = isreal(A);
    isRealB = isreal(B);
    if isRealA ..
     & isRealB then
        X = linalg_dgesv(A, B);
    else
        A = linalg_complexify(A);
        B = linalg_complexify(B);
        X = linalg_zgesv(A, B);
    end
endfunction

