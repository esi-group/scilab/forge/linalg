// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function L = linalg_chol(A)
    // Computes the Cholesky decomposition.
    //
    // Calling Sequence
    //   L = linalg_chol(A)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // L : a n-by-n matrix of doubles, lower triangular so that A = L*L'
    //
    // Description
    // Returns the Cholesky decomposition A = L*L' where L is lower triangular.
    // Assumes that A is symetric positive definite.
    //
    // Examples
    // C = [1 0 0 0
    //      1 2 0 0
    //      1 2 3 0
    //      1 2 3 4];
    // A = C * C';
    // L = linalg_chol(A)
    // ulperror = (L(L<>0)-C(C<>0)) ./ C(C<>0) / %eps
    //
    // // The Cholesky decomposition can fail if the
    // // matrix is not positive definite.
    // A = matrix(1:16, 4, 4)
    // L = linalg_chol(A)
    //
    // // Using the decomposition to solve Ax=b
    // // Source: https://ece.uwaterloo.ca/~dwharder/NumericalAnalysis/04LinearAlgebra/cholesky/
    // A = [0.9  0.06 -0.39 -0.24
    //      0.06 1.604 0.134 0.464
    //     -0.39 0.134 2.685 0.802
    //     -0.24 0.464 0.802 1.977];
    // L = linalg_chol(A)
    // b = [0.063
    //     -0.6358
    //      0.5937
    //     -0.1907]
    // y = L \ b
    // x = L' \ y
    // // Compare with backslash
    // e = A \ b
    // ulperror = (x-e) ./ e / %eps
    //
    // // A Cholesky decomposition
    // // Source: https://ece.uwaterloo.ca/~dwharder/NumericalAnalysis/04LinearAlgebra/cholesky/
    // A = [5 1.2 0.3 -0.6
    //      1.2 6 -0.4 0.9
    //      0.3 -0.4 8 1.7
    //     -0.6 0.9 1.7 10];
    // L = linalg_chol(A)
    //
    // // See the algorithm
    // edit linalg_chol
    //
    // Authors
    // Copyright (C) 2010 - 2011 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   Golub and Van Load, algorithm 4.2.1 (Cholesky, gaxpy version)

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_chol", rhs, 1);
    apifun_checklhs("linalg_chol", lhs, 0:1);

    // Check types
    apifun_checktype("linalg_chol", A, "A", 1, "constant");

    // Proceed...
    [m n] = size(A);
    assert_checkequal(m, n);

    for j = 1:n
        if j > 1 then
            A(j:n, j) = A(j:n, j) - A(j:n, 1:j-1) * A(j, 1:j-1)';
        end
        if A(j, j) <= 0 ..
         | isnan(A(j, j)) then
            error(msprintf("%s: The matrix is not positive definite.", ..
                           "linalg_chol"));
        end
        A(j,j ) = sqrt(A(j, j));
        A(j+1:n, j) = A(j+1:n, j) / A(j, j);
    end
    L = tril(A);
endfunction

