// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = linalg_gausspivotalnaive(varargin)
    // Computes the solution of a linear equation with Gauss and row pivoting.
    //
    // Calling Sequence
    //   x = linalg_gausspivotalnaive(A, b)
    //   x = linalg_gausspivotalnaive(A, b, verbose)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // b : a n-by-1 matrix of doubles
    // verbose : a 1-by-1 matrix of boolean(default verbose = %f),
    //           set to true to display intermediate messages
    // x : a n-by-1 matrix of doubles
    //
    // Description
    //   Returns the solution of Ax = b
    //   with a Gauss elimination and row exchanges.
    //   This algorithm might be sensitive to inaccuracies if A near singular.
    //   There is no solution is A is singular.
    //   If A is not ill-conditionned, the algorithm is accurate.
    //   Uses a naive algorithm with all loops expanded(no vectorization).
    //
    // Examples
    // A = [3 17  10
    //      2  4  -2
    //      6 18 -12];
    // b = [67
    //      4
    //      6];
    // x = linalg_gausspivotalnaive(A, b);
    // xe = [1
    //       2
    //       3];
    // // See what happens
    // x = linalg_gausspivotalnaive(A, b, %t);
    //
    // // See the algorithm
    // edit linalg_gausspivotalnaive
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_gausspivotalnaive", rhs, 2:3);
    apifun_checklhs("linalg_gausspivotalnaive", lhs, 0:1);

    // Get arguments
    A  = varargin(1);
    b  = varargin(2);
    verbose = apifun_argindefault(varargin, 3, %f);

    // Check types
    apifun_checktype("linalg_gausspivotalnaive", A, "A", 1, "constant");
    apifun_checktype("linalg_gausspivotalnaive", b, "b", 2, "constant");
    apifun_checktype("linalg_gausspivotalnaive", verbose, "verbose", 3, "boolean");

    // Check size
    [nrows ncols] = size(A);
    apifun_checksquare("linalg_gausspivotalnaive", A, "A", 1);
    apifun_checkvector("linalg_gausspivotalnaive", b, "b", 2);
    apifun_checkdims("linalg_gausspivotalnaive", b, "b", 2, [nrows, 1]);
    apifun_checkscalar("linalg_gausspivotalnaive", verbose, "verbose", 3);

    // Proceed...

    n = size(A, "r");

    // Initialize permutation
    P = zeros(1, n-1);

    // Perform Gauss transforms
    for k = 1:n-1
        // Search pivot
        mu = k;
        abspivot = abs(A(mu, k));
        for i = k:n
            if abs(A(i, k)) > abspivot then
                mu = i;
                abspivot = abs(A(i, k));
            end
        end
        if verbose then
            printf("=============================\n");
            printf("Column to search for pivot : [%s]\n", ..
                   strcat(string(A(k:n, k)), " "));
            printf("Pivot at index %d = %f\n", mu, A(mu, k));
            printf("Swap rows %d <-> %d\n", mu, k);
        end

        // Swap lines k and mu from columns k to n
        for j = k:n
            tmp = A(k, j);
            A(k, j) = A(mu, j);
            A(mu, j) = tmp;
        end
        P(k) = mu;

        // Perform transform for lines k+1 to n
        for i = k+1:n
            A(i, k) = A(i, k) / A(k, k);
        end
        for i = k+1:n
            for j = k+1:n
                A(i, j) = A(i, j) ..
                        - A(i, k) * A(k, j);
            end
        end
        if verbose then
            printf("Step #%d / %d\n", k, n);
            printf("A:");
            disp(A);
        end
    end

    // Perform forward substitution
    for k = 1:n-1
        // Swap b(k) and b(P(k))
        tmp = b(k);
        mu = P(k);
        b(k) = b(mu);
        b(mu) = tmp;
        // Substitution
        for i = k+1:n
            b(i) = b(i) - b(k) * A(i, k);
        end
    end
    if verbose then
        printf("After forward loop, b:\n");
        disp(b);
    end

    // Perform backward substitution
    for k = n:-1:1
        t = 0;
        for j = k+1:n
            t = t + A(k, j) * b(j);
        end
        b(k) = (b(k) - t) / A(k, k);
    end
    if verbose then
        printf("After backward loop, b:\n");
        disp(b);
    end
    x = b;
endfunction

