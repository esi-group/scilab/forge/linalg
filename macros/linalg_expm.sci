// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function F = linalg_expm(A)
    // Computes the exponential of a matrix.
    //
    // Calling Sequence
    //   B = linalg_expm(A)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // B : a n-by-n matrix of doubles, B = exp(A)
    //
    // Description
    //   Scales the matrix by power of 2 to increase convergence.
    //   Uses Padé approximants.
    //
    // Examples
    // A = [1 2
    //      3 4];
    // expected = [
    //     5.196895619870500838D+01    7.473656456700319950D+01
    //     1.121048468505048135D+02    1.640738030492098005D+02
    // ];
    // F = linalg_expm(A);
    // ulperror = ceil(abs(F-expected) ./ abs(expected) / %eps)
    //
    // // Test #2 : a 3x3 matrix
    // A = matrix((1:9),3,3);
    // expected ..
    // = [1118906.699413182214 2533881.041898961645 3948856.384384742938
    //    1374815.062935801689 3113415.031380542088 4852012.999825284816
    //    1630724.426458421396 3692947.020862122998 5755170.615265826695];
    // F = linalg_expm(A);
    // ulperror = ceil(abs(F-expected) ./ abs(expected) / %eps)
    // // Test #3 : from wikipedia
    // A = [21 17  6
    //      -5 -1 -6
    //       4  4 16];
    // expected = 1/4 ..
    //          * [13*exp(16)-exp(4) 13*exp(16)-5*exp(4)  2*exp(16)-2*exp(4)
    //             -9*exp(16)+exp(4) -9*exp(16)+5*exp(4) -2*exp(16)+2*exp(4)
    //             16*exp(16)        16*exp(16)           4*exp(16)];
    // F = linalg_expm(A);
    // ulperror = ceil(abs(F-expected) ./ abs(expected) / %eps)
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   "Matrix Computations", Golub and Van Loan, Third Edition,
    //   The Johns Hopkins University Press, p573, Algorithm 11.3-1.
    //   http://en.wikipedia.org/wiki/Matrix_exponential

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_expm", rhs, 1);
    apifun_checklhs("linalg_expm", lhs, 0:1);

    // Check types
    apifun_checktype("linalg_expm", A, "A", 1, "constant");

    // Proceed...

    // Scale A by power of 2 so that its norm is < 1/2 .
    j = max(0, 1 + floor(log2(norm(A, "inf"))));
    A = A / 2^j;

    // Main loop
    X = eye(A);
    c = 1;
    N = eye(A);
    D = eye(A);
    q = 7;
    for k = 1:q
        c = c * (q-k+1) / ((2*q-k+1)*k);
        X = A * X;
        cX = c * X;
        N = N + cX;
        if modulo(k, 2) == 1 then
            cX = -cX;
        end
        D = D + cX;
    end

    // Solve D*F= N for F
    F = D \ N;

    // Undo scaling by repeated squaring
    for k = 1:j
        F = F * F;
    end
endfunction

