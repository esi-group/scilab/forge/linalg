// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = linalg_gausspivotal(varargin)
    // Computes the solution of a linear equation with Gauss and row pivoting.
    //
    // Calling Sequence
    //   x = linalg_gausspivotal(A, b)
    //   x = linalg_gausspivotal(A, b, verbose)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // b : a n-by-1 matrix of doubles
    // verbose : a 1-by-1 matrix of boolean (default verbose = %f),
    //           set to %t to display intermediate messages
    // x : a n-by-1 matrix of doubles
    //
    // Description
    //   Returns the solution of Ax = b
    //   with a Gauss elimination and row exchanges.
    //   This algorithm might be sensitive to inaccuracies if A near singular.
    //   There is no solution is A is singular.
    //   If A is not ill-conditionned, the algorithm is accurate.
    //   Uses an effective algorithm with vectorization.
    //
    // Examples
    // A = [3 17 10
    //      2 4 -2
    //      6 18 -12];
    // b = [67
    //      4
    //      6];
    // x = linalg_gausspivotal(A, b);
    // xe = [1
    //       2
    //       3];
    // // See what happens
    // x = linalg_gausspivotal(A, b, %t);
    //
    // // See the algorithm
    // edit linalg_gausspivotal
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_gausspivotal", rhs, 2:3);
    apifun_checklhs("linalg_gausspivotal", lhs, 0:1);

    // Get arguments
    A = varargin(1);
    b = varargin(2);
    verbose = apifun_argindefault(varargin, 3, %f);

    // Check types
    apifun_checktype("linalg_gausspivotal", A, "A", 1, "constant");
    apifun_checktype("linalg_gausspivotal", b, "b", 2, "constant");
    apifun_checktype("linalg_gausspivotal", verbose, "verbose", 3, "boolean");

    // Check size
    [nrows ncols] = size(A);
    apifun_checksquare("linalg_gausspivotal", A, "A", 1);
    apifun_checkvector("linalg_gausspivotal", b, "b", 2);
    apifun_checkdims  ("linalg_gausspivotal", b, "b", 2, [nrows 1]);
    apifun_checkscalar("linalg_gausspivotal", verbose, "verbose", 3);

    // Proceed...

    n = size(A, "r");
    // Initialize permutation
    P = zeros(1, n-1);
    // Perform Gauss transforms
    for k = 1:n-1
        // Search pivot
        [abspivot, murel] = max(abs(A(k:n, k)));
        // Shift mu, because max returns with respect to the column (k:n, k)
        mu = murel + k - 1;
        if verbose then
            printf("=============================\n");
            printf("Column to search for pivot : [%s]\n", ..
                   strcat(string(A(k:n, k)), " "));;
            printf("Pivot at index %d = %f\n", mu, A(mu, k));
            printf("Swap rows %d <-> %d\n", mu, k);
        end
        // Swap lines k and mu from columns k to n
        A([k mu], k:n) = A([mu k], k:n);
        P(k) = mu;
        // Perform transform for lines k+1 to n
        A(k+1:n, k)     = A(k+1:n, k) / A(k, k);
        A(k+1:n, k+1:n) = A(k+1:n, k+1:n) ..
                        - A(k+1:n, k) * A(k, k+1:n);
        if verbose then
            printf("Step #%d / %d\n", k, n);
            printf("A:");
            disp(A);
        end
    end
    // Perform forward substitution
    for k = 1:n-1
        // Swap b(k) and b(P(k))
        mu = P(k);
        b([k mu]) = b([mu k]);
        // Substitution
        b(k+1:n) = b(k+1:n) - b(k) * A(k+1:n, k);
    end
    if verbose then
        printf("After forward loop, b:\n");
        disp(b);
    end
    // Perform backward substitution
    if n > 0 then
		b(n) = b(n) / A(n, n);
		for k = n-1:-1:1
			b(k) = (b(k) - A(k, k+1:n) * b(k+1:n)) / A(k, k);
		end
	end
    if verbose then
        printf("After backward loop, b:\n");
        disp(b);
    end
    x = b;
endfunction

