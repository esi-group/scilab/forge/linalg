// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [L, U, P] = linalg_factorlupivot(varargin)
    // Computes the LU decomposition with pivoting.
    //
    // Calling Sequence
    //   [L, U, P] = linalg_factorlupivot(A)
    //   [L, U, P] = linalg_factorlupivot(A, verbose)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // verbose : a 1-by-1 matrix of boolean (default verbose=%f),
    //           set to %t to display intermediate messages
    // L : a n-by-n matrix of doubles, lower triangular.
    // U : a n-by-n matrix of doubles, upper triangular.
    // P : a n-by-n matrix of doubles, with 0 or 1 entries
    //
    // Description
    //   Decomposes A as P*A = L*U with
    //   L lower triangular and unit diagonal,
    //   U upper triangular and non-unit diagonal,
    //   P a permutation matrix with 0/1 entries.
    //   This algorithm might be sensitive to inaccuracies if A near singular.
    //   There is no solution is A is singular.
    //   If A is not ill-conditionned, the algorithm is accurate.
    //   Uses an effective algorithm with vectorization.
    //
    // Examples
    // A = [1d-8  1
    //      1     1];
    // [L U P] = linalg_factorlupivot(A)
    // Lexpected = [1     0
    //              1d-8  1];
    // Uexpected = [1     1
    //              0     1 - 1.d-8];
    // Pexpected = [0  1
    //              1  0];
    // // See what happens
    // [L U P] = linalg_factorlupivot(A, %t)
    //
    // // See the algorithm
    // edit linalg_factorlupivot
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_factorlupivot", rhs, 1:2);
    apifun_checklhs("linalg_factorlupivot", lhs, 0:3);

    // Get arguments
    A  = varargin(1);
    verbose = apifun_argindefault(varargin, 2, %f);

    // Check types
    apifun_checktype("linalg_factorlupivot", A, "A", 1, "constant");
    apifun_checktype("linalg_factorlupivot", verbose, "verbose", 2, "boolean");

    // Check size
    apifun_checkscalar("linalg_factorlupivot", verbose, "verbose", 2);

    n = size(A, "r");

    // Initialize permutation
    T = 1:n;

    // Perform Gauss transforms
    for k = 1:n-1
        // Search pivot
        [abspivot, murel] = max(abs(A(k:n, k)));

        // Shift mu, because max returns with respect to the column (k:n,k)
        mu = murel + k - 1;
        if verbose then
            printf("=============================\n");
            printf("Column to search for pivot : [%s]\n", ..
                   strcat(string(A(k:n, k)) , " "));
            printf("Pivot at index %d = %f\n", mu, A(mu, k));
            printf("Swap rows %d <-> %d\n", mu, k);
        end

        // Swap lines k and mu from columns 1 to n
        A([k mu], 1:n) = A([mu k], 1:n);
        T([k mu]) = T([mu k]);

        // Perform transform for lines k+1 to n
        A(k+1:n, k)     = A(k+1:n, k) / A(k, k);
        A(k+1:n, k+1:n) = A(k+1:n, k+1:n) - A(k+1:n, k) * A(k, k+1:n);
        if verbose then
            printf("Step #%d / %d\n", k, n);
            printf("A:");
            disp(A);
            printf("L:");
            disp(tril(A, -1) + eye());
            printf("U:");
            disp(triu(A, 0));
        end
    end

    L = tril(A, -1) + eye();
    U = triu(A, 0);
    P = eye(n, n);
    P = P(T, :);
endfunction

