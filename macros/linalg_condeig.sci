// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [R, D, kappa] = linalg_condeig(A)
    // Computes the condition number of the eigenvalues of a matrix.
    //
    // Calling Sequence
    //   kappa = linalg_condeig(A)
    //   [R, D] = linalg_condeig(A)
    //   [R, D, kappa] = linalg_condeig(A)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // R : a n-by-n matrix of doubles, the right eigenvectors
    // D : a n-by-n matrix of doubles, the diagonal eigenvalue matrix
    // kappa : a n-by-1 matrix of doubles, the condition number
    //         of all eigenvalues
    //
    // Description
    //   Computes the sensitivity of the eigenvalues with respect to changes in
    //   the matrix.
    //
    // Examples
    // A = [-149   -50  -154
    //       537   180   546
    //       -27    -9   -25];
    // kappa = linalg_condeig(A)
    // [R D kappa] = linalg_condeig(A)
    // kappa_expected = [603.6390
    //                   395.2366
    //                   219.2920]
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    // "Matrix Computations", G.H. Golub and C.F. Van Loan,
    // Second Edition, 1989, sec. 7.2.2.
    // "Numerical Computing with Matlab", Cleve Moler

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_condeig", rhs, 1);
    apifun_checklhs("linalg_condeig", lhs, 0:3);

    // Check types
    apifun_checktype("linalg_condeig", A, "A", 1, "constant");

    // Proceed...

    [R D] = spec(A);
    L = inv(R);
    for k = 1 : size(D, "r")
        y = L(k, :);
        x = R(:, k);
        kappa(k) = norm(y) * norm(x) / norm(y*x);
    end

    if lhs == 1 then
        R = kappa;
    end
endfunction

