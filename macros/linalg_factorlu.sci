// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [L, U] = linalg_factorlu(varargin)
    // Computes the LU decomposition without pivoting.
    //
    // Calling Sequence
    //   [L, U] = linalg_factorlu(A)
    //   [L, U] = linalg_factorlu(A, verbose)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // verbose : a 1-by-1 matrix of boolean (default verbose=%f),
    //           set to %t to display intermediate messages
    // L : a n-by-n matrix of doubles, lower triangular.
    // U : a n-by-n matrix of doubles, upper triangular.
    //
    // Description
    //   Decomposes A as A = L*U with
    //   L lower triangular and unit diagonal,
    //   U upper triangular and non-unit diagonal.
    //   This algorithm might be sensitive to inaccuracies if A near singular.
    //   There is no solution is A is singular.
    //   The algorithm might be unstable, even if A is not ill-conditionned.
    //   Indeed, it might produce large entries in L and U, even
    //   if entries in A are of moderate size.
    //   Uses an effective algorithm with vectorization.
    //
    // Examples
    // // Basic test
    // A = [1 1 1 1 1
    //      1 2 2 2 2
    //      1 2 3 3 3
    //      1 2 3 4 4
    //      1 2 3 4 5];
    // [L, U] = linalg_factorlu(A);
    // Lexpected = [1 0 0 0 0
    //              1 1 0 0 0
    //              1 1 1 0 0
    //              1 1 1 1 0
    //              1 1 1 1 1];
    // Uexpected = [1 1 1 1 1
    //              0 1 1 1 1
    //              0 0 1 1 1
    //              0 0 0 1 1
    //              0 0 0 0 1];
    // // See what happens
    // [L, U] = linalg_factorlu(A, %t);
    //
    // // Combine factorlu and solvelu
    // A = [1 1 1 1 1
    //      1 2 2 2 2
    //      1 2 3 3 3
    //      1 2 3 4 4
    //      1 2 3 4 5];
    // b = [5
    //      9
    //      12
    //      14
    //      15];
    // [L U] = linalg_factorlu(A);
    // x = linalg_solvelu(L, U, b, %f);
    // xexpected = [1
    //              1
    //              1
    //              1
    //              1];
    //
    // // A difficult case: pivoting is necessary
    // A = [1d-8  1
    //      1     1];
    // [L, U] = linalg_factorlu(A);
    // Lexpected = [1.   0
    //              1d8  1.];
    // Uexpected = [1d-8   1
    //              0.    -1.d8+1];
    // cond(A) // 2.6180340238745070102766
    // cond(L) // 9999999949752408.
    // cond(U) // 9999999900000000.
    //
    // // See the algorithm
    // edit linalg_factorlu
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010-2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs("linalg_factorlu", rhs, 1:2);
    apifun_checklhs("linalg_factorlu", lhs, 2:2);

    // Get arguments
    A  = varargin(1);
    verbose = apifun_argindefault(varargin, 2, %f);

    // Check types
    apifun_checktype("linalg_factorlu", A, "A", 1, "constant");
    apifun_checktype("linalg_factorlu", verbose, "verbose", 2, "boolean");

    // Check size
    apifun_checkscalar("linalg_factorlu", verbose, "verbose", 2);

    // Proceed...

    n = size(A, "r");
    // Perform Gauss transforms
    for k = 1:n-1
        if verbose then
            printf("=============================\n");
            printf("Pivot at index %d = %s\n", k, string(A(k, k)));
        end
        // Perform transform for lines k+1 to n
        A(k+1:n, k)     = A(k+1:n, k) / A(k, k);
        A(k+1:n, k+1:n) = A(k+1:n, k+1:n) ..
                        - A(k+1:n, k) * A(k, k+1:n);
        if verbose then
            printf("Step #%d / %d\n", k, n);
            printf("A:");
            disp(A);
            printf("L:");
            disp(tril(A, -1) + eye());
            printf("U:");
            disp(triu(A, 0));
        end
    end
    L = tril(A, -1) + eye();
    U = triu(A, 0);
endfunction

