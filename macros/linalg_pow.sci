// Copyright (C) 2010 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function B = linalg_pow(A, p)
    // Computes A^p
    //
    // Calling Sequence
    //  B = linalg_pow(A, p)
    //
    // Parameters
    // A : a n-by-n matrix of doubles. May be dense, real, complex or sparse.
    // p : a 1-by-1 matrix of doubles, integer value, positive.
    // B : a n-by-n matrix of doubles, B = A^p
    //
    // Description
    //   Uses a binary algorithm to compute the power of the matrix A.
    //   This algorithm requires generally log2(p) iterations, which
    //   is (much) faster that Scilab v5 power algorithm for real matrices.
    //   It is based on a macro.
    //
    // Examples
    // A = [5 4 3 2 1
    //      4 4 3 2 1
    //      0 3 3 2 1
    //      0 0 2 2 1
    //      0 0 0 1 1];
    // B = linalg_pow(A, 5);
    // E = [37721 47455 45750 34540 18160
    //      33940 42751 41245 31150 16380
    //      15420 19695 19156 14525 7650
    //      3720  5010  5020  3861  2045
    //      360   570   620   495   266];
    //
    // // With a complex matrix
    // A = (3  - 2 * %i)* testmatrix("frk", 5);
    // A^6
    // linalg_pow(A, 6);
    //
    // // Measure the difference between Scilab's pow and this pow
    // p = 99999;
    // tic(); A^p; toc()                  // Typically: 2.7 s
    // tic(); linalg_pow(A, p); toc()     // Typically: 0.003 s
    // tic(); linalg_powfast(A, p); toc() // Typically: 0.002 s
    //
    // Authors
    // Copyright (C) 2009 - Vladimir Sergievskiy
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    // D. Knuth, "The Art of Computer Programming", Vol. 2,
    // "Seminumerical Algorithms"
    // http://bugzilla.scilab.org/show_bug.cgi?id=8428

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_pow", rhs, 2:2);
    apifun_checklhs("linalg_pow", lhs, 0:1);

    // Check type
    apifun_checktype("linalg_pow", A, "A", 1, "constant");
    apifun_checktype("linalg_pow", p, "p", 2, "constant");

    // Check size
    apifun_checkflint("linalg_pow", p, "p", 2);
    apifun_checkgreq("linalg_pow", p, "p", 2, 1);

    // Proceed...
    B = eye(A);
    while p > 0
        r = modulo(p, 2);
        if r == 1 then
            B = A * B;
            p = p - 1;
        end
        A = A * A;
        p = p / 2;
    end
endfunction

