// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <string.h>
// Headers from Scilab:
#include "machine.h"
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"
// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/***************************************/
/*                                     */
/*     Usage: C = linalg_zhbev(AB)     */
/*                                     */
/***************************************/

// AB must be a complex hermitian band matrix A converted in diagonal format,
// using the convention :
// --------------------------------
// for j from 1 to size(A)
//     m = min(size(A),j+nsubdiag)
//     for i from j to m
//         AB(1+i+j, j) = A(i, j)
// -------------------------------
// where nsubdiag is the number of non-null subdiagonals of A.
// C is the list of eigenvalues of A, sorted in ascending order.
// Example:
// AB = [1 2 3
//       4 5 0
//       6 0 0];
// linalg_zhbev(AB)
// ans =
//     - 4.094602
//     - 2.0337914
//      12.128393

int sci_linalg_zhbev(GW_PARAMETERS)
{
    SciErr sciErr;
    int ldab;
    int n;
    int kd;
    int mAB;
    int nAB;
    int one = 1;
    int AB = 1;
    int W = 1;
    int Z = 2;
    int WORK = 3;
    int RWORK = 4;
    int * piAddr = NULL;
    int iComplex = 0;
    int iType = 0;
    int LWORKMIN;
    int LWORK;
    int INFO;
    double * lW = NULL;
    double * lRWORK = NULL;
    double * lrAB = NULL;
    double * liAB = NULL;
    double * lrZ = NULL;
    double * liZ = NULL;
    double * lrWORK = NULL;
    double * liWORK = NULL;
    doublecomplex * lAB = NULL;
    doublecomplex * lZ = NULL;
    doublecomplex * lWORK = NULL;

    CheckRhs(1, 1);
    CheckLhs(0, 1);

    // get variable address of the first input argument

    sciErr = getVarAddressFromPosition(pvApiCtx, AB, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    // check type and complexity
    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (iType != sci_matrix)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d. "
                        "Matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    iComplex = isVarComplex(pvApiCtx, piAddr);
    if (iComplex == 0)
    {
        Scierror(999, _("%s: Wrong content for input argument #%d. "
                        "Complex matrix expected.\n"),
                 fname, 1);
        return 1;
    }

    // get size and data from Scilab memory
    sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, &mAB, &nAB, &lrAB, &liAB);      /*      AB     */
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    ldab = mAB;
    n = nAB;
    kd = ldab-1;

    lAB = oGetDoubleComplexFromPointer(lrAB, liAB, ldab * n);

    // Create W
    sciErr = allocMatrixOfDouble(pvApiCtx, Rhs+W, n, one, &lW);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    // Create Z
    sciErr = allocComplexMatrixOfDouble(pvApiCtx, Rhs+Z, one, one, &lrZ, &liZ);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    lZ = oGetDoubleComplexFromPointer(lrZ, liZ, one * one);

    // Create WORK

    sciErr = allocComplexMatrixOfDouble(pvApiCtx,Rhs+WORK, n, one, &lrWORK, &liWORK);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    lWORK = oGetDoubleComplexFromPointer(lrWORK, liWORK, n * one);

    // Create RWORK
    LWORKMIN = Max(1, 3*(n-2));

    sciErr = allocMatrixOfDouble(pvApiCtx, Rhs+RWORK, LWORKMIN, one, &lRWORK);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    C2F(zhbev)("N", "L", &n, &kd, lAB, &ldab, lW, lZ,
               &one, lWORK, lRWORK, &INFO);

    if (INFO != 0)
    {
        //C2F(errorinfo)("zhbev ", &INFO, 5L);
        char * fname = "zhbev";
        unsigned long fname_len = 5L;
        int i = 0;
        #define nlgh 24
        char Fname[nlgh+1];
        int minlength = Min(fname_len, nlgh);
        strncpy(Fname, fname, minlength);
        Fname[minlength] = '\0';
        for (i =  0;
             i <  (int)minlength;
             i += 1)
        {
            if (Fname[i] == ' ')
            {
                Fname[i] = '\0';
                break;
            }
        }
        Scierror(998, _("%s: internal error, info=%d.\n"), Fname, INFO);
        return 1;
    }

    LhsVar(1) = Rhs + W;

    vFreeDoubleComplexFromPointer(lAB);
    vFreeDoubleComplexFromPointer(lZ);
    vFreeDoubleComplexFromPointer(lWORK);

    return 0;
}
