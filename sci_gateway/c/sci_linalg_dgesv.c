// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <stdlib.h>

// Headers from Scilab:
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"

// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/*********************************/
/*                               */
/*     X = linalg_dgesv(A,B)     */
/*                               */
/*********************************/

int sci_linalg_dgesv(GW_PARAMETERS)
{
    int MA = 0, NA = 0;
    int MB = 0, NB = 0;

    int * piAddr = NULL;
    SciErr sciErr;
    int iRet = 0;
    double * lrA = NULL;
    double * lrB = NULL;
    int * ipiv = NULL;

    int info = 0;

    CheckRhs(2, 2);
    CheckLhs(0, 1);

    // Get A
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 1, piAddr, &lrA, &MA, &NA, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    if (NA != MA)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "A square matrix expected.\n"),
                 fname, 1);
        return 1;
    }

    // Get B
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 2, piAddr, &lrB, &MB, &NB, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    if (MB != MA)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 2, MA, NB);
        return 1;
    }
    // Create array ipiv
    ipiv = (int*)malloc(MA * sizeof(int));
    if (ipiv == NULL)
    {
        Scierror(999, _("%s: No more memory.\n"), fname);
        return 1;
    }
    // Call dgesv
    C2F(dgesv)(&MB, &NB, lrA, &MA, ipiv, lrB, &MB, &info);
    if (info != 0)
    {
        Scierror(999, _("%s: Matrix is singular.\n"), fname);
        return 1;
    }
    // Free memory
    free(ipiv);

    LhsVar(1) = 2;

    return 0;
}

