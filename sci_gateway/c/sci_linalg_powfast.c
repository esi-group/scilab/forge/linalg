// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Headers from Scilab:
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "version.h"
#if SCI_VERSION_MAJOR > 5
#include "sci_malloc.h"
#else
#include "MALLOC.h"
#endif
#include "linalg.h"

// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/***********************************/
/*                                 */
/*     B = linalg_powfast(A,p)     */
/*                                 */
/***********************************/

int sci_linalg_powfast(GW_PARAMETERS)
{
    SciErr sciErr;
    int nRowsA, nColsA;
    int nRowsp, nColsp;
    int nRowsB, nColsB;
    int iType = 0;
    int * piAddr = NULL;
    double * A = NULL;
    double * p = NULL;
    double * B = NULL;
    int ip;

    CheckRhs(2, 2);
    CheckLhs(0, 1);

    // Get 1st argument : A
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (iType != sci_matrix)
    {
        Scierror(204, _("%s: Wrong type for input argument #%d: "
                        "Matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &nRowsA, &nColsA, &A);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (nRowsA != nColsA)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "A square matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    // Get 2nd argument : p
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (iType != sci_matrix)
    {
        Scierror(204, _("%s: Wrong type for input argument #%d: "
                        "Matrix expected.\n"),
                 fname, 2);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &nRowsp, &nColsp, &p);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (nRowsp != 1
     || nColsp != 1)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "A real scalar expected.\n"),
                 fname, 2);
        return 1;
    }

    // Create output argument y in Scilab
    nRowsB = nRowsA;
    nColsB = nColsA;
    sciErr = allocMatrixOfDouble(pvApiCtx, Rhs+1, nRowsB, nColsB, &B);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    // Compute B = A^p
    ip = (int)p[0];
    linalg_powfast(A, nRowsA, ip, B);

    LhsVar(1) = Rhs + 1;
    return 0;
}

