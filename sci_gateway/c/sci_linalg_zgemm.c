// Copyright (C) INRIA
// Copyright (C) 2010 - DIGITEO - Allan CORNET
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Headers from Scilab:
#include "machine.h"
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"

// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

// Example:
// alfa = 1+%i;
// betha = alfa;
// A = rand(2, 2) + %i * rand(2, 2);
// B = A;
// C = A;
/*******************************************/
/*                                         */
/*     D = zgemm(alfa, A, B, betha, C)     */
/*                                         */
/*******************************************/

int sci_linalg_zgemm(GW_PARAMETERS)
{
    int m = 0, n = 0;
    int k = 0;
    int mA = 0, nA = 0;
    int mB = 0, nB = 0;
    int mC = 0, nC = 0;

    int * piAddr = NULL;
    double alphar, alphai;
    double betar, betai;
    SciErr sciErr;
    int iRet = 0;
    double * lrA = NULL, * liA = NULL;
    double * lrB = NULL, * liB = NULL;
    double * lrC = NULL, * liC = NULL;
    int iType = 0;
    int iComplex = 0;
    doublecomplex * lalpha = NULL;
    doublecomplex * lbeta = NULL;
    doublecomplex * lA = NULL;
    doublecomplex * lB = NULL;
    doublecomplex * lC = NULL;
    double * lrD = NULL;
    double * liD = NULL;

    CheckRhs(5, 5);
    CheckLhs(0, 1);

    // Get alpha (arg #1)
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getScalarComplexDouble(pvApiCtx, piAddr, &alphar, &alphai);
    if (iRet)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 1, 1, 1);
        return 1;
    }
    lalpha = oGetDoubleComplexFromPointer(&alphar, &alphai, 1);

    // Get A (arg #2)
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getComplexMatrixOfDoubles(fname, 2, piAddr, &lrA, &liA,
                                     &mA, &nA, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    lA = oGetDoubleComplexFromPointer(lrA, liA, mA*nA);

    // Get B (arg #3)
    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getComplexMatrixOfDoubles(fname, 3, piAddr, &lrB, &liB,
                                     &mB, &nB, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    lB = oGetDoubleComplexFromPointer(lrB, liB, mB*nB);

    // Get beta (arg #4)
    sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getScalarComplexDouble(pvApiCtx, piAddr, &betar, &betai);
    if (iRet)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 1, 1, 1);
        return 1;
    }
    lbeta = oGetDoubleComplexFromPointer(&betar, &betai, 1);

    // Get C (arg #5)
    sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getComplexMatrixOfDoubles(fname, 5, piAddr, &lrC, &liC,
                                     &mC, &nC, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    lC = oGetDoubleComplexFromPointer(lrC, liC, mC*nC);

    m = mA;
    n = nB;
    if (nA != mB
     || mA != mC
     || nB != nC)
    {
        Scierror(999, "%f: invalid matrix dims\n", fname);
        return 1;
    }

    // Call zgemm
    k = nA;
    C2F(zgemm)("n", "n", &m, &n, &k, lalpha, lA,
               &mA, lB, &mB, lbeta, lC, &mC);

    // Create output variable: D
    sciErr = allocComplexMatrixOfDouble(pvApiCtx, Rhs + 1, mC, nC, &lrD, &liD);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    // Transfert eigenvalues from doublecomplex to real and imaginary parts
    vGetPointerFromDoubleComplex(lC, nC*mC, lrD, liD);

    LhsVar(1) = Rhs+1;

    vFreeDoubleComplexFromPointer(lA);
    vFreeDoubleComplexFromPointer(lB);
    vFreeDoubleComplexFromPointer(lC);

    return(0);
}

