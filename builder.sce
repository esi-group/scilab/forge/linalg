// Copyright (C) 2008 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function linalg_build()

    mode(-1);
    lines(0);

    // Uncomment this line to make a debug version of the Toolbox
    //setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")

    TOOLBOX_NAME = "linalg";
    TOOLBOX_TITLE = "Linalg";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_macros(toolbox_dir);
    tbx_builder_src(toolbox_dir);
    tbx_builder_gateway(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

endfunction

linalg_build();
clear linalg_build

