// Copyright (C) 2009-2010 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef _LINALG_H_
#define _LINALG_H_

#ifdef _MSC_VER
    #if LIBLINALG_EXPORTS
        #define LINALG_IMPORTEXPORT __declspec (dllexport)
    #else
        #define LINALG_IMPORTEXPORT __declspec (dllimport)
    #endif
#else
    #define LINALG_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

LINALG_IMPORTEXPORT void linalg_powfast(double * A, int n, int p, double * B);

__END_DECLS

#endif /* _LINALG_H_ */

